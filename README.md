# inspection_planner_msgs

Contains all the messages for the inspection/planning pipeline


## How do I install this package?
Add this package to your ROS workspace:
```bash
cd ~/<your-workspace>/src/
git clone https://gitlab.com/navfac_raven_osu/inspection_planner_msgs.git
catkin build
source ../devel/setup.bash


## Who do I talk to?

- Chris Lee (leeyuh@oregonstate.edu)
- Graeme Best (bestg@oregonstate.edu)


